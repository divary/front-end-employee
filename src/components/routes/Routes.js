import React, {Component} from "react";
import { Switch, Route } from "react-router-dom";
import Employee from "../employee/Employee";

class Routes extends Component {
    render() {
        return(
            <Switch>
                {/*<Route exact path ="/" render = {() =>*/}
                {/*    <p>Home Page</p>*/}
                {/*} />*/}
                <Route exact path ="/" render = {() => <Employee/>} />
                <Route path ="/employers" render = {() => <Employee/>} />
            </Switch>
        )
    }
}

export default Routes;
