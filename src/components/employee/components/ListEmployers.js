import React, {Component, useEffect, useState, useReducer} from "react";
import Card from "reactstrap/es/Card";
import CardHeader from "reactstrap/es/CardHeader";
import Table from "reactstrap/es/Table";
import {connect} from "react-redux";
import Spinner from "reactstrap/es/Spinner";
import {destroyEmployee, getEmployeeUnDeleted, updateEmployee} from "../services/EmployeeService";
import {Link} from "react-router-dom";
import {withRouter} from "react-router-dom"
import FaIcon from "../../../shared/fontawesome/FaIcon";
import {Button, Modal, ModalHeader, ModalBody, ModalFooter, Col} from 'reactstrap';
import Swal from 'sweetalert2';
import {EmployeeReducer, initialState} from "../reducers/EmployeeReducer";
import Page from "../../../shared/pagination/Page";

const ListEmployers = props => {

    const [state, dispatch] = useReducer(EmployeeReducer, initialState);

    const {page} = props;

    useEffect(() => {
        loadData()
    },[state.employers]);

    const loadData = () => {
        const {fetchData, fetchComplete, page} = props;
        fetchData();
        getEmployeeUnDeleted(page).then((employersUnDeleted)=>{
            // console.log(employersUnDeleted)
            fetchComplete(employersUnDeleted);
        });
    }

    const handleDelete = async (employee) => {
        console.log(employee)
        employee.remove = true;
        console.log(employee)
        return await updateEmployee(employee)
            .then((isSuccess) => {
                console.log("ISSUCCC",isSuccess)
                if (isSuccess) {
                    loadData();
                    return true
                }
            }).catch(() => {
                console.log("EROR")
                return false
            })
    }

    const handleDestroy = (employeeId) => {
        destroyEmployee(employeeId)
            .then((isSuccess) => {
                if (isSuccess) {
                    loadData();
                    return true
                }
                return false
            });
    }

    const handleEdit = (employeeId) => {
        console.log(employeeId);
        const {handleEditButton, history} = props;
        handleEditButton(employeeId);
        history.replace("/employers/form");
    }

    const handlePage = (page) => {
        const {handlePagination} = props;
        handlePagination(page+1);
        loadData()
    }

    const Pag = () => {
        return (
            Page()
        )
    }

    const handleDeleteButton = async (employee) => {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        })

        swalWithBootstrapButtons.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
        }).then((result) => {
            if (result.value) {
                if (handleDelete(employee)){
                    swalWithBootstrapButtons.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                } else {
                    swalWithBootstrapButtons.fire(
                        'Error!',
                        'Your file not deleted.',
                        'error'
                    )
                }
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })

    }

    const generateTableRows = () => {
        const {employers} = props;
        let rows = <tr><td colSpan="2" className="text-center"> <Spinner color="primary" /> </td> </tr>
        if(!props.isLoading){
            rows = employers.map((employee, index)=>{
                // console.log(employee)
                return(
                    <tr key={index}>
                        <td>{index+1}</td>
                        <td>{employee.name}</td>
                        <td>{employee.birthDate}</td>
                        <td>{employee.position.name}</td>
                        <td>{employee.idNumber}</td>
                        <td>{employee.gender}</td>
                        <td>
                            <Button type="button" color="warning" size="sm" className="shadow" onClick={() => handleEdit(employee.id)}>
                                <FaIcon icon="far edit"/> Edit</Button>
                        </td>
                        <td>
                            <Button variant="danger" color="danger" size="sm" className="shadow" onClick={() => { handleDeleteButton(employee)}}>
                                <FaIcon icon="fa trash-alt" /> Delete</Button>
                        </td>
                    </tr>
                )
            });
        }
        return rows;

    }

        return(
            <>
                <Card className="shadow">
                    <CardHeader tag="strong">Karyawan
                        <Link to="/employers/form"><Button className="float-right" size="sm" color="primary">
                            <FaIcon icon="fas plus-square" /> Add employee</Button>
                        </Link>
                    </CardHeader>
                    <Table responsive striped hover className="m-0">
                        <thead>
                        {/*<th width="5%">#</th>*/}
                        <th width="2%">No</th>
                        <th width="20%">Nama</th>
                        <th width="15%">Taggal Lahir</th>
                        <th width="10%" >Jabatan</th>
                        <th width="15%">Nip</th>
                        <th width="15%">Jenis Kelamin</th>
                        <th colSpan="2" width="20%" className="text-center">Aksi</th>
                        </thead>
                        <tbody>
                        {generateTableRows()}
                        </tbody>
                    </Table>
                </Card>
                <Button type="submit" color="primary" onClick={() => handlePage(page)}>
                    <FaIcon icon="fas save"/>
                </Button>
                {/*{Pag()}*/}
            </>
        )
}

function mapStateToProps(state) {
    return{...state};
}
function mapDispatchToProps(dispatch) {
    return{
        fetchData:() => dispatch({type: "setLoading"}),
        fetchComplete:(payload) => dispatch({type: "fetchComplete", payload}),
        handleEditButton:(payload) => dispatch({type: "editEmployee", payload}),
        handlePagination:(payload) => dispatch({type: "setPage", payload}),
        // handleDelete : (payload) => dispatch ({ type: DELETE_ALBUM, payload}),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ListEmployers));
