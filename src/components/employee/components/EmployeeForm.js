import React, {Component, Fragment, useReducer, useState, useEffect} from "react";
import {Col, Form, FormGroup, Label, Input, Button, Card, CardHeader, CardBody} from "reactstrap";
import {
    checkNip,
    createEmployee,
    getEmployeeUnDeleted,
    getPositionUnDeleted,
    updateEmployee
} from "../services/EmployeeService";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom"
import Swal from 'sweetalert2';
import {Toast} from '../../../shared/sweetAlert2/toast/Toast';
import FaIcon from "../../../shared/fontawesome/FaIcon";

const EmployeeForm = props => {

    const [checkAvailableNIP, setCheckAvailableNIP] = useState(false);

    useEffect(() => {
        loadData()
    },[]);

    const loadData = () => {
        const {fetchData, fetchComplete, fetchCompletePosition} = props;
        fetchData();
        getEmployeeUnDeleted().then((employersUnDeleted)=>{
            fetchComplete(employersUnDeleted);
        });
        getPositionUnDeleted().then((positionUnDeleted)=>{
            fetchCompletePosition(positionUnDeleted);
        });
    }

    const submitEmployeeData = async () => {
        const {form} = props;
        if ( form.id !== undefined) return await updateEmployee(form)
        return await createEmployee(form)
    }

    const handleSubmitButton = (event) => {

        event.preventDefault();

        Swal.fire({
            title: 'Are you sure?',
            text: "You will save employees with a name test",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, save it!'
        }).then((result) => {
            if (result.value) {

                handleSubmit()
                // Swal.fire(
                //     'Saved!',
                //     'Employee has been deleted.',
                //     'success'
                // )
            }
        })

    }

    const saved = () => {
        Swal.fire(
            'Saved!',
            'Employee has been deleted.',
            'success'
        )
    }

    const handleReturn = () => {
        const {history, resetForm} = props;
        resetForm();
        history.replace("/employers");
    }

    const handleCheckNip = async () => {
        const {form} = props;
        // await checkNip(form.idNumber)
        if (checkAvailableNIP) {
            setCheckAvailableNIP(false)
        } else {
            setCheckAvailableNIP(
                await checkNip(form.idNumber).then((id) => {
                    if (id == form.id || id.length == 0){
                        Toast("success", "Succes")
                        return true
                    }
                    Toast("error", "Nip sudah terdaftar")
                    return false
                })
            )
        }
    }

    const handleSubmit = () => {
        const {setLoading, submitComplete, history} = props;
        setLoading();
        submitEmployeeData()
            .then((data) => {
                console.log('sasas',data)
                saved();
                submitComplete();
                history.replace('/employers');
            }).catch((e) => {
            console.log(e)
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong!',
            })
        })
    }

    const isValid = () => {
        const {form, isLoading} = props;
        // console.log('birth', form.birthDate.length == 10)
        // console.log('posisi', form.position.id.length != 0)
        // console.log('number', form.idNumber.length >= 3)
        // console.log('gender', form.gender.length != 0)
        // return true;
        return (
            form.name.trim().length > 0  &&
            form.name.trim().length < 100 &&
            form.birthDate.length == 10 &&
            form.position.id.length != 0 &&
            form.idNumber.length >= 4 &&
            checkAvailableNIP &&
            form.gender.length != 0
        )
    }

        const {form, position, handleInputChanges, isLoading, fromLoading} = props
        return (
            <Fragment>
                <Card>
                    <CardBody>
                        <Form onSubmit={(event) => handleSubmitButton (event)}>
                            <FormGroup row>
                                <Label for="name" sm="3">Nama</Label>
                                <Col sm="9">
                                    <Input type="text" id="name" name="name" multiline value={form.name} placeholder="Masukan Nama" onChange={(event) => handleInputChanges('name', event.target.value)} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="birthDate" sm="3">Tanggal Lahir</Label>
                                <Col sm="3">
                                    <Input type="date" min="1900" id="birthDate" name="birthDate" value={form.birthDate} placeholder="Masukan Tanggal Lahit" onChange={(event) => handleInputChanges('birthDate', event.target.value)} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="position" sm="3">Jabatan</Label>
                                <Col sm="9" md={2}>
                                    <Input type="select" className={"input-sm"}  id="position" name="position" value={form.position.id} onChange={(event) => handleInputChanges(["position","id"], event.target.value)}>
                                        <option value="" hidden>Pilih Jabatan</option>
                                        {
                                            position.map(position => {
                                                return <option value={position.id}>{position.name}</option>
                                            })
                                        }
                                    </Input>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="idNumber" sm="3">NIP</Label>
                                <Col sm="9" md={2}>
                                    <Input type="number" disabled={checkAvailableNIP} min="0" max="999999999" className={"input-sm"}  id="idNumber" name="idNumber" value={form.idNumber} placeholder="Masukan NIP" onChange={(event) => handleInputChanges('idNumber', event.target.value)} />
                                </Col>
                                <Button type="button" color="primary" onClick={handleCheckNip}>
                                    <FaIcon icon="fas save"/>
                                    {
                                        checkAvailableNIP
                                            ? ' Edit NIP' : ' Check NIP'}
                                </Button>
                            </FormGroup>
                            <FormGroup row>
                                <Label for="gender" sm="3">Jenis Kelamin</Label>
                                <Col sm="9" md={5}>
                                    <Input type="select" className={"input-sm"}  id="gender" name="gender" value={form.gender} defaultValue = "Male" onChange={(event) => handleInputChanges('gender', event.target.value)}>
                                        <option value="" hidden>Pilih Jenis Kelamin</option>
                                        {
                                            [ "Male", "Female"].map(gender => {
                                                return <option value={gender.toUpperCase()}>{gender}</option>
                                            })
                                        }
                                    </Input>
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Col sm={{ size:9, offset:3}}>
                                    <td>
                                    <Button type="submit" color="primary" disabled={!isValid()}>
                                        <FaIcon icon="fas save"/>
                                        {
                                            isLoading
                                            // false
                                            ? ' Submiting Data...' : ' Save Employee'}
                                    </Button>
                                    </td>
                                    <td>
                                    <Button type="reset" color="dark" onClick={handleReturn}>
                                        <FaIcon icon="fas undo"/> Return</Button>
                                    </td>
                                </Col>
                            </FormGroup>
                        </Form>
                    </CardBody>
                </Card>
            </Fragment>
        );
    }
// }

function mapStateToProps(state) {
    return {...state}
}

function mapDispatchToProps(dispatch) {
    return {
        handleInputChanges: (inputName, inputValue)=> dispatch({type : "handleInputChange", payload: {inputName, inputValue}}),
        setLoading: () => dispatch({type: "setLoading"}),
        resetForm: () => dispatch({type: "resetForm"}),
        submitComplete: () => dispatch({type: "submitComplete"}),
        formLoading: () => dispatch({type: "formLoading"}),
        fetchComplete:(payload) => dispatch({type: "fetchComplete", payload}),
        fetchCompletePosition:(payload) => dispatch({type: "fetchCompletePosition", payload}),
        fetchData:() => dispatch({type: "setLoading"}),

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(EmployeeForm));
