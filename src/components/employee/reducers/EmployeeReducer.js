const defaultFormValues = {
    id: undefined,
    name: '',
    birthDate: '',
    position: {
        id : ''
    },
    idNumber: '',
    gender: '',
    remove: false
}
const initialState = {
    isLoading: true,
    employers: [],
    position: [],
    page: 1,
    size: 5,
    form: { ...defaultFormValues, position : {...defaultFormValues.position}},
}

function EmployeeReducer(state = initialState, action) {
    const {type, payload} = action;

    switch (type) {
        case "editEmployee":
            const employee = state.employers.find((employee) => employee.id === payload);
            let editEmployee = {...employee};
            return {...state, form: editEmployee};

        case "resetForm":
            return { ...state, form: {...defaultFormValues}}

        case "setLoading":
            return { ...state, isLoading: true}

        case "fetchComplete":
            return { ...state, isLoading: false, employers: [...payload]}

        case "fetchCompletePosition":
            return { ...state, isLoading: false, position: [...payload]}

        case "handleInputChange":
            const {form} = state
            const {inputName, inputValue} = payload
            // console.log(form)
            if (Array.isArray(inputName))
                form[inputName[0]][inputName[1]] = inputValue;
            else
            form[inputName] = inputValue;
            return {...state,form: {...form}};

        case "submitComplete":
            return {...state, isLoading: false, form: {...defaultFormValues}};

        case "formLoading":
            return { ...state, isLoading: false}

        case "setPage":
            return { ...state, page: payload}

        default:
            return {...state};
    }
}

export {EmployeeReducer, initialState};
