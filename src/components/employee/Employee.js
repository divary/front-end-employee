import React, {Component} from "react";
import {createStore} from "redux";
import {Provider} from "react-redux";
import {Row, Col} from "reactstrap";
import ListEmployers from "./components/ListEmployers";
import {EmployeeReducer} from "./reducers/EmployeeReducer";
import {Route} from "react-router-dom";
import EmployeeForm from "./components/EmployeeForm";

const employeeStore = createStore(EmployeeReducer)

class Employee extends Component{
    render() {
        return(
            <Provider store={employeeStore}>
                <Row>
                    <Col>
                        <Route exact path ="/employers" render={() => <ListEmployers/>}/>
                        <Route path ="/employers/form" render={() => <EmployeeForm/>}/>
                    </Col>
                </Row>
            </Provider>
        )
    }
}

export default Employee;
