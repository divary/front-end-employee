import client from "../../../shared/http-client/client";

export async function createEmployee(employee) {
    const  {data} = await client.post('/employee',employee);
    console.log("data",data)
    return data;
}

export async function getSingleEmployee(id) {
    const {data} = await  client.get(`/employee/${id}`);
    return data;
}

export async function checkNip(nip) {
    const {data} = await  client.get(`/employee/check/${nip}`);
    // const {data} = await  client.get(`/employee/check/12345`);
    console.log("data ",data)
    return data;
}

export async function getEmployeeUnDeleted(page) {
    console.log("INIADALAHG PAGE", page)
    const {data:{content, pageable, totalPages}} =await client.get(`/employee/UnDeleted?page=${page}&size=5`);
    // const data =await client.get('/employee/UnDeleted?page=1&size=50');
    console.log(pageable)
    return content;
}

export async function getPositionUnDeleted() {
    const {data:{content}} =await client.get('/position/UnDeleted?page=1&size=50');
    // const {data:{content}} =await client.get('/album?page=1&size=50');
    return content;
}

export async function updateEmployee(employee) {
    const {data} = await client.put('/employee', employee);
    return data;
}

export async function destroyEmployee(employeeId) {
    const response = await client.delete(`/employee/${employeeId}`);

    console.log(response.status);
    if (response.status === 200) return true;
    else return false;
}
