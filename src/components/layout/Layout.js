import React, {Component, Fragment} from "react";
import {Row, Col} from "reactstrap";
import Routes from "../routes/Routes";

class Layout extends Component {
    render() {
        return (
            <Fragment>
                <Row>
                    <Col sm="10">
                        <Routes/>
                    </Col>
                </Row>
            </Fragment>
        )
    }
}

export default Layout;
