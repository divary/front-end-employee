import React from 'react';
import '../App.css';
import {Container} from "reactstrap";
import {BrowserRouter} from "react-router-dom";
import Layout from "./layout/Layout";


function App() {
  return (
    <Container fluid>
      <BrowserRouter>
          <Layout/>
      </BrowserRouter>
    </Container>
  );
}

export default App;
