import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import {getEmployeeUnDeleted} from "../../components/employee/services/EmployeeService";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";

const Page = (props) => {

    const loadData = () => {
        const {fetchData, fetchComplete} = props;
        fetchData();
        getEmployeeUnDeleted(2).then((employersUnDeleted)=>{
            // console.log(employersUnDeleted)
            fetchComplete(employersUnDeleted);
        });
    }


    return (
        <Pagination aria-label="Page navigation example">
            <PaginationItem>
                <PaginationLink first href="#" />
            </PaginationItem>
            <PaginationItem>
                <PaginationLink previous href="#" />
            </PaginationItem>
            <PaginationItem>
                <PaginationLink href="#">
                    1
                </PaginationLink>
            </PaginationItem>
            <PaginationItem>
                <PaginationLink href="#">
                    2
                </PaginationLink>
            </PaginationItem>
            <PaginationItem>
                <PaginationLink href="#">
                    3
                </PaginationLink>
            </PaginationItem>
            <PaginationItem>
                <PaginationLink href="#">
                    4
                </PaginationLink>
            </PaginationItem>
            <PaginationItem>
                <PaginationLink href="#">
                    5
                </PaginationLink>
            </PaginationItem>
            <PaginationItem>
                <PaginationLink next onClick={loadData} />
            </PaginationItem>
            <PaginationItem>
                <PaginationLink last href="#" />
            </PaginationItem>
        </Pagination>
    );
}

function mapStateToProps(state) {
    return{...state};
}
function mapDispatchToProps(dispatch) {
    return{
        fetchData:() => dispatch({type: "setLoading"}),
        fetchComplete:(payload) => dispatch({type: "fetchComplete", payload}),
    }
}

// export default Page;

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Page));

