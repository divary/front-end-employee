const {createProxyMiddleware} = require('http-proxy-middleware');
module.exports=function (app) {
    app.use(
        '/api',
        createProxyMiddleware({
            // target:'http://10.10.13.252:8080/',
            target:'http://localhost:8080/',
            changeOrigin:true,
            pathRewrite:{'^/api':''}
        })
    )
}
